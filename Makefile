VERSION=`git describe --tags`
NAME=csv-events-to-ndjson

build-docker:
	docker build . -t $(NAME):$(VERSION) -t $(NAME):latest

test-docker:
	docker run --rm -it -v `pwd`:/code $(NAME):$(VERSION) python3 -m pytest

test:
	python3 setup.py test

install:
	python3 setup.py install
