"""
Convert csv to events, one row = 1 ndjson
"""

import csv
import json
import os
from typing import Callable, Union, Dict, Any
from pathlib import Path

JsonDict = Dict[str, Any]

class EventsFileDispatcher:
    def __init__(self,
                 outdir: Union[Path, str],
                 suffix: str='.ndjson',
                 mode: str='w'):
        self.outdir = Path(outdir)
        if not self.outdir.is_dir():
            print("Creating", self.outdir)
            self.outdir.mkdir()
        self.suffix = suffix
        self.mode = mode
        self._open_files = {}

    def __enter__(self):
        return self

    def close_opened_files(self):
        for f in self._open_files.values():
            f.close()

    def __exit__(self, *args):
        self.close_opened_files()

    def get_event_file(self, event_name: str):
        try:
            return self._open_files[event_name]
        except KeyError:
            new_fh = (self.outdir / (event_name + self.suffix)).open(self.mode)
            self._open_files[event_name] = new_fh
            return new_fh

    def write_event(self, event_name: str, event: JsonDict):
        self.get_event_file(event_name).write(json.dumps(event) + "\n")


def identity(event: JsonDict) -> JsonDict:
    """An identity function returning the same event

    This can be replaced with a custom cleaning logic
    """
    return event

def process_csvs(
        event_type_column: str,
        indir: Union[Path, 'str']=Path('/data/in/tables/'),
        outdir: Union[Path, 'str']=Path('/data/out/files/'),
        in_glob_pattern: str='**/*.csv',
        transform: Callable=identity):

    with EventsFileDispatcher(outdir=outdir) as out:
        for path in Path(indir).glob(in_glob_pattern):
            print("Parsing", path, "for events.")
            with path.open() as fin:
                r = csv.DictReader(fin)
                for event in r:
                    out.write_event(event[event_type_column], transform(event))
