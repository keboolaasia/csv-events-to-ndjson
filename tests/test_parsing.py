import pytest
from pathlib import Path
from csvtondjson import EventsFileDispatcher, process_csvs


def assert_output(out):
    out = Path(out)
    assert (out / 'subscribe.ndjson').is_file()
    assert len((out / 'subscribe.ndjson').read_text().splitlines()) == 1

    assert (out / 'unsubscribe.ndjson').is_file
    assert len((out / 'unsubscribe.ndjson').read_text().splitlines()) == 2


def test_dispatching_events(tmpdir):
    events = [
        {"action":"subscribe", "id":"1", "year":"2018", "value":"42"},
        {"action":"unsubscribe", "id":"2", "year":"2018", "value":"666"},
        {"action":"unsubscribe", "id":"3", "year":"2018", "value":"Alpha"},
    ]
    outdir = tmpdir.mkdir('out')
    with EventsFileDispatcher(outdir=outdir.strpath) as dis:
        for event in events:
            dis.write_event(event['action'], event)

    out = Path(outdir.strpath)
    assert_output(out)

@pytest.fixture
def events_csv_dir(tmpdir):
    indir = tmpdir.mkdir('in').mkdir('tables')
    indir.join('events.csv').write('''action,id,year,value
subscribe,1,2018,42
unsubscribe,2,2018,666
unsubscribe,3,2018,Alpha''')
    return indir


@pytest.mark.parametrize('in_glob_pattern',
                         [
                             '**/*.csv',
                             'events.csv'
                         ])
def test_processing_csvs(tmpdir, events_csv_dir, in_glob_pattern):
    outdir = tmpdir.mkdir("out").mkdir("files")


    process_csvs('action',
                 indir=events_csv_dir.strpath,
                 outdir=outdir.strpath,
                 in_glob_pattern=in_glob_pattern)
    assert_output(outdir.strpath)
