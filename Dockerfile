FROM python:3.7-alpine
RUN mkdir -p /data/in/files /data/in/tables /data/out/tables /code
ENV COMPONENT_DATADIR /data/
WORKDIR /code
RUN pip install pytest && apk add --no-cache-dir git

COPY . /code

RUN pip install .

CMD csvtondjson '/data/'
