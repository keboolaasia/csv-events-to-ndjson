from setuptools import setup, find_packages

setup(
    name='csvtondjson',
    use_scm_version=True,
    setup_requires=['setuptools-scm', 'pytest-runner'],
    url='https://bitbucket.org/keboolaasia/csv-events-to-ndjson/',
    packages=find_packages(exclude=['tests']),
    test_suite='tests',
    scripts=['bin/csvtondjson'],
    tests_require=['pytest']
)
